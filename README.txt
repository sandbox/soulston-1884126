Lexis Webform Email

This module is useful if you are using a webform to take a signup and ultimately create a user from the form. The module's main code involves a lookup on the user table to check to see if the given email address is already in use on the system:

SELECT COUNT(uid) FROM {users} WHERE mail = :email LIMIT 1

You can then use a rule to create the user.